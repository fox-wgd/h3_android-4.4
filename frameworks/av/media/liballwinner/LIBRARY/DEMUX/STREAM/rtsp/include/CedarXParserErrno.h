#ifndef CEDARX_PARSER_ERRNO_H
#define CEDARX_PARSER_ERRNO_H

enum CedarXParserErrno
{
    CEDARX_PARSER_ERR_BASE = 0x00467000,
        /*随便定义的，后续每个模块应该统一*/
    CEDARX_PARSER_ERR_CACHE_NOTREADY,
};

#endif
