LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

# <modulename>:<filename>
LOCAL_PREBUILT_LIBS := \
    libavutil:libavutil.so \
    libavformat:libavformat.so \
    libavcodec:libavcodec.so \

LOCAL_MODULE_TAGS := optional
include $(BUILD_MULTI_PREBUILT)
