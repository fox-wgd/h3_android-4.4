/** 
 * @file swavstage.h
 * @brief 媒体数据播放录制模块
 * @author Yeshengnan/Hujinshui/Chenkai/Dou hongchen
 * @date 2010-11-20
 * @version copy from swdec.h
 */

#ifndef __SWAVSTAGE_H__
#define __SWAVSTAGE_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "swplayerdef.h"
#include "mediastream.h"

/**
 * @AVStage 启动模式
 */
typedef enum
{
	E_AVSSTARTMODE_NONE = 0x0,  /**<不启动任何设备*/
	E_AVSSTARTMODE_DEC,         /**<启动解码器*/
	E_AVSSTARTMODE_REC,         /**<启动录制器*/
	E_AVSSTARTMODE_DECREC,      /**<启动解码器和录制器*/
	E_AVSSTARTMODE_MAX

} e_avs_startmode;


/**
 * @ 当前DRM的类型
 */
typedef enum
{
    DRMTYPE_UNKNOWN,
    DRMTYPE_IRDETOCA,
    DRMTYPE_VERIMERTRIX,
    DRMTYPE_PVR,
} swavs_drmtype_e;


/**
 * @ AVS_PROPERTY_SETKEY的传入信息
 */
typedef struct _swavs_keyinfo 
{
    int oe;
    char *key;
    int key_size;
} swavs_keyinfo_t;


/**
 * @ AVS_PROPERTY_AUDIORANGE的传入信息
 */
typedef struct _swavs_audiorange 
{
    int audio_min;
    int audio_max;
} swavs_audiorange_t;


/**
 *@解码器事件定义
 */
typedef enum swavs_event {
	AVS_EVENT_ERROR,		  /**<系统错误事件*/
	AVS_EVENT_VIDEOSTREAMINFO,/**<视频解析出码流信息*/
	AVS_EVENT_VIDEOFIRSTPTS,  /**<视频解码出第一帧*/
	AVS_EVENT_VIDEOSYNC,	  /**<视频已经锁定上系统时钟[STC]*/
	AVS_EVENT_VIDEOUNDERFLOW, /**<视频下溢*/
	AVS_EVENT_VIDEOPTSERROR,  /**<视频时间戳发生错误*/
	AVS_EVENT_VIDEOSIZECHANGE,  /**<视频帧分辨率发生了变化*/
	AVS_EVENT_AUDIOSTREAMINFO,/**<音频解析出码流信息*/
	AVS_EVENT_AUDIOFIRSTPTS,  /**<音频解码出第一帧*/
	AVS_EVENT_AUDIOSYNC,	  /**<音频已经锁定上系统时钟[STC]*/
	AVS_EVENT_AUDIOUNDERFLOW, /**<音频下溢*/
	AVS_EVENT_AUDIOPTSERROR,  /**<音频时间戳发生错误*/
	AVS_EVENT_RECBEGIN,  	  /**<录制正常启动*/
	AVS_EVENT_RECSTOP,  	  /**<录制正常启动*/
	AVS_EVENT_RECERROR,  	  /**<录制正常启动*/
	AVS_EVENT_RECRECVSIZE,   /**<录制正常启动*/
	AVS_EVENT_PLAYBEGIN,   /**<录制正常启动*/
	AVS_EVENT_PLAYEND,   /**<录制正常启动*/
	AVS_EVENT_CODECNOTSUPPORT,/**音/视频编码格式不支持解码*/
	AVS_EVENT_CODECNOLICENSE, /**音/视频权限没有打开*/
	AVS_EVENT_VIDEOREVERT,	  /**<视频恢复解码*/
	AVS_EVENT_AUDIOREVERT, 	  /**<音频恢复解码*/
	AVS_EVENT_TIMESHIFT_NEW_TIMEDUR,  	  /**<ttnet flash空间不足修改时移窗口>*/
	AVS_EVENT_ADEC_DOLBY_DUAL_MONO,  	  /**dolby dual mono*/
}swavs_event_e;

/**
 * @音视频同步模式
 */
typedef enum{
	AVS_SYNC_NULL,		/**<不开同步*/
	AVS_SYNC_VIDEO,		/**<以视频PTS做参考*/
	AVS_SYNC_AUDIO,		/**<以音频PTS做参考*/
	AVS_SYNC_FIRST,		/**<音、视频PTS都参考,哪个有效,用哪个*/
	AVS_SYNC_PCR		/**<PCR更新STC*/
}swavs_sync_e;

/**
 * @brief 				事件回调处理函数指针
 * @param handle  	 	用户句柄
 * @param event_type 	事件类型 
 * @param wparam 		用户参数
 * @param lparam 		触发event事件同时上报信息, 比如,AVS_EVENT_AUDIOPTSERROR, lparam 为错误的pts
 * @param lextend 		扩展参数
 */
typedef int (*avs_event_callback)(void *handle, swavs_event_e event_type, uint32_t wparam, uint32_t lparam, uint32_t lextend);


/**
 * @缓冲区属性
 */
typedef struct _swfifoinfo 
{
    unsigned int size;	/**<缓冲区总大小*/
    unsigned int depth; /**<当前数据量*/
    
} swfifoinfo_t;

/**
 *@媒体消费器状态
 */
typedef enum
{
    AVS_STATUS_NULL = 0,
    AVS_STATUS_PLAY,			/**<播放状态,通过param来标记速率*/
    AVS_STATUS_PREPAUSE,		/**<预暂停*/
    AVS_STATUS_PAUSED,			/**<暂停*/
    AVS_STATUS_SEEK,			/**<SEEK*/
    AVS_STATUS_PRESTOP,		    /**<预停止*/

	AVS_STATUS_DEC_START,		/**<启动解码器 */
	AVS_STATUS_DEC_PRESTOP,		/**<预停止解码器*/
	AVS_STATUS_DEC_STOP,		/**<停止解码器 */

	AVS_STATUS_REC_START,		/**<启动录制器 */
	AVS_STATUS_REC_PRESTOP,		/**<预停止录制器 */
	AVS_STATUS_REC_STOP,		/**<停止录制器 */

	AVS_STATUS_TIMESHIFT_START,	 /**<启动本地时移*/
	AVS_STATUS_TIMESHIFT_PRESTOP,/**<预停止本地时移 */
	AVS_STATUS_TIMESHIFT_STOP,	 /**<停止本地时移 */

    //!!!!!! 注意: 修改时须同步更新sw_avstage_status2str

    AVS_STATUS_MAX,

} swavs_status_e;


typedef enum vframerate
{
    VFRAMERATE_UNKNOWN = 0,
    VFRAMERATE_23_976,
    VFRAMERATE_24,
    VFRAMERATE_25,
    VFRAMERATE_29_97,
    VFRAMERATE_30,
    VFRAMERATE_50,
    VFRAMERATE_59_94,
    VFRAMERATE_60,
    VFRAMERATE_MAX

} swvframerate_e;


/**
 * @视频解码器状态
 */
typedef struct videostatus
{
	swavs_status_e estatus;	  	/**<当前工作状态*/
	unsigned int width;			/**<图像宽度*/
	unsigned int height;		/**<图像高度*/
	unsigned int pts;			/**<45K计数*/
    unsigned int stc;           /**<stc*/
	unsigned int errcnt;  		/**<解码出错数*/
	unsigned int decodercnt;	/**<解码帧数*/
	swvframerate_e framerate; 	/**<视频帧率*/
	bool is_interlaced;		    /**<是否交织*/
    swfifoinfo_t fifoinfo;		/**<缓冲区信息*/
    swaspectratio_e aspect;

} videoinfo_t;

/**
 * @音频解码器状态
 */
typedef struct audiostatus
{
	swavs_status_e estatus;  /**<当前工作状态*/
	unsigned int samplerate; /**<声音采样率*/
	int channels;			 /**<声音通道数*/
	int bitspersample;  	 /**<每个采样的bit数目*/
	unsigned int pts;		 /**音频PTS 45K*/
    unsigned int stc;        /**<stc*/
	swfifoinfo_t fifoinfo;	 /**<缓冲区信息*/

} audioinfo_t;

/**
 * @录制器状态
 */
typedef struct recstatus
{
	swavs_status_e estatus;  /**<当前工作状态*/
	swfifoinfo_t fifoinfo;	 /**<缓冲区信息*/
	unsigned int datatime;   /**<录制节目时长*/

} recinfo_t;

/**
 * @抓屏信息
 */
typedef struct capimgstatus
{
	int width;
	int height;
	char* outbuf;
} capimginfo_t;

typedef enum {
    SWAVS_STRUCTURE_3D_TOP_AND_BOTTOM = 0,
    SWAVS_STRUCTURE_3D_SIDE_BY_SIDE, 
    SWAVS_STRUCTURE_3D_CLOSE, 
} swavs_3d_mode_e;

/**
 *@ AVStage propertyid
 */
typedef enum
{
    AVS_PROPERTY_NULL = 0,
    AVS_PROPERTY_VPID,          	/**< Video PID */
    AVS_PROPERTY_APID,          	/**< Audio PID */
    AVS_PROPERTY_PPID,          	/**< Pcr PID */
	AVS_PROPERTY_SPID,          	/**< Subtitle PID */
    AVS_PROPERTY_VPTS,          	/**< current video pts */
    AVS_PROPERTY_APTS,          	/**< current audio pts */
	AVS_PROPERTY_VCODEC,        	/**< current video codec*/
	AVS_PROPERTY_ACODEC,        	/**< current audio codec*/  
    AVS_PROPERTY_REALTIME_STREAM,	/**< 实时流?用来决定同步方式 */
    AVS_PROPERTY_VIDEO_RENDER,      /**< sw_videorender_e */
	/**
	 *@音频控制
	 */
    AVS_PROPERTY_AUDIORANGE,        /**< 声音的范围, 参数为swavs_audiorange_t */
    AVS_PROPERTY_AUDIO_CHANNEL, 	/**< 声道*/
    AVS_PROPERTY_AUDIO_VOLUME,  	/**< 音量*/
    AVS_PROPERTY_AUDIO_MUTE,    	/**< 静音*/
    AVS_PROPERTY_AUDIO_ATTACH,  	/**< 加载音频解码器*/
    AVS_PROPERTY_AUDIO_DETACH,  	/**< 卸载音频解码器*/
	AVS_PROPERTY_AUDIO_TRACK,   	/**< 音轨*/
    AVS_PROPERTY_AUDIO_RENDER,      /**< sw_audiorender_e */
	/**
	 *@视频输出控制
	 */
    AVS_PROPERTY_VIDEO_RECT,		/**<视频输出位置*/
    AVS_PROPERTY_VIDEO_ORDER,  
    AVS_PROPERTY_VIDEO_MODE,		/**<视频输出模式:pig ,full ,pig &full */     
	AVS_PROPERTY_VIDEO_VISIBLE, 
	AVS_PROPERTY_VIDEO_CONTENTMODE, /**<视频源输出模式: full, letter box ...*/
	AVS_PROPERTY_VIDEO_ALPHA,		/**<视频ALPHA值*/
	AVS_PROPERTY_VIDEO_ASPECTRATIO, /**<输出比例 4:3 ?16:9 ...*/
    AVS_PROPERTY_VIDEO_BRIGHTNESS,  /**<亮度 */
    AVS_PROPERTY_VIDEO_CONTRAST,    /**<对比度 */
    AVS_PROPERTY_VIDEO_SATURATION,  /**<饱和度 */
    AVS_PROPERTY_VIDEO_CLEAR,       /**<清除stop状态下由于上次stop时hold last pic而保留的画面*/
    AVS_PROPERTY_VIDEO_ERRHANDLING, /**<丢帧时的处理方式, 类型为swvdec_errhanding_e*/
	/**
	 *@PVR属性
	 */
	AVS_PROPERTY_REC_PATH,      	/**<录制存放路径*/
	AVS_PROPERTY_REC_INPUT,      	/**<录制类型*/
	AVS_PROPERTY_REC_TIMESHIFT, 	/**<本地时移*/
	AVS_PROPERTY_REC_SEEKRANGE, 	/**<本地时移seek 范围*/
	AVS_PROPERTY_REC_DATATIME,  	/**<本地时移节目时间*/
	AVS_PROPERTY_REC_TIMEDUR,  	/**<本地时移节目时间*/
		
	AVS_PROPERTY_VIDEOINFO,   		/**<节目视频信息*/
	AVS_PROPERTY_AUDIOINFO,   		/**<节目音频信息*/
	AVS_PROPERTY_RECINFO,   		/**<录制器状态*/
    AVS_PROPERTY_FIFOINFO,			/**<缓冲区状态*/
    
	AVS_PROPERTY_SETKEY,      		/**<设置解密key*/    
    AVS_PROPERTY_PLAYTIME,    		/**< current audio pts */

    //ATTENTION 为了避免相关库的重新编译, 此定义放到最后
    AVS_PROPERTY_VIDEO_TARGET,		/**<视频窗口, 平台相关, 如android的surface*/

	AVS_PROPERTY_CAPTURE_VIDEOIMAGE, /*抓取视频帧*/
    AVS_PROPERTY_TIMESHIFT_INFO,	/**<本地时移转录制信息*/
    AVS_PROPERTY_REC_OVERLAP_INFO,		/**<重叠录制信息*/
	AVS_PROPERTY_PVR_RRECHANDLE, /*快进快退平滑播放获取rrec句柄*/
    //!!!!!! 注意: 修改时须同步更新sw_avstage_property2str
   
    AVS_PROPERTY_MAX
} swavs_property_e;

typedef enum _player_property
{
    PLAYER_PROPERTY_NULL = AVS_PROPERTY_MAX + 1,
    PLAYER_PROPERTY_RECVTIMEOUT,
    PLAYER_PROPERTY_SUB_LANG,
    PLAYER_PROPERTY_SUB_SHOW,
    PLAYER_PROPERTY_SUB_TRACK,
    PLAYER_PROPERTY_MPEGTSINFO,
#ifdef SUPPORT_VERIMATRIX
	PLAYER_PROPERTY_VM_HANDLE,
#endif
    PLAYER_PROPERTY_MAX,
} e_player_property;

typedef struct _swavstage swavstage_t;

/**
 * @brief AVStage初始化
 * @return 0:成功，负值:失败
 */
int sw_avstage_init();

/**
 * @brief AVStage退出
 * @return 无
 */
void sw_avstage_release();

/** 
 * 打开
 * @param cap [in] 所需功能描述
 * 
 * @return 句柄， NULL:失败
 */
swavstage_t* sw_avstage_open(const mediacap_t *cap);

/** 
 * 关闭
 * @param avsobj [in] 句柄
 * 
 * @return 无
 */
void sw_avstage_close(swavstage_t *avsobj);

/** 
 * @brief sw_avstageset_caps设置player的能力, 该能力可能当前未使用 
 * 
 * @param caps由e_mediacap定义的比特位集, 可为0表示先部分配资源
 * 
 * @return 成功返回0, 失败返回-1
 */
int sw_avstage_set_caps(swavstage_t *avsobj, const mediacap_t *cap);

/** 
 * @brief sw_avstageget_caps读取player的能力, 该能力可能当前未使用 
 * 
 * @param caps输出参数, 成功时将能力集填充到此参数中
 * 
 * @return 成功返回0, 失败返回-1
 */
int sw_avstage_get_caps(swavstage_t *avsobj, mediacap_t *cap/*out*/);

/** 
 * @brief sw_avstageget_inuse_caps获取当前使用中的能力 
 * 
 * @param caps输出参数, 成功时将能力集填充到此参数中
 * 
 * @return 成功返回0, 失败返回-1
 */
int sw_avstage_get_inuse_caps(swavstage_t *avsobj, mediacap_t *cap);

/**
 * @brief 			启动avstage
 * @param avsobj 	avstage 对象句柄 
 * @param media  	需要展示的媒体流信息
 * @param startmode 启动模式
 * @return 0 成功 	其它 错误号
 */
int sw_avstage_start(swavstage_t* avsobj, media_stream_t* media, e_avs_startmode startmode, uint32_t wparam);

/**
 * @brief 				停止avstage
 * @param avsobj 		avstage 对象句柄 
 * @param hold_lastpic  是否保留最后一帧
 * @param startmode	 	启动模式
 * @return 				0 成功 其它 错误号
 */
int sw_avstage_stop(swavstage_t* avsobj, bool hold_lastpic);


/**
 * @brief 				检查缓冲数据是否消耗完毕
 * @param avsobj 		avstage 对象句柄 
 
 * @return true 消耗完毕, 否则 缓冲区中还有数据
 */
bool sw_avstage_is_over(swavstage_t* avsobj);

/**
 * @brief 				从AVstage 中获取空闲缓冲
 * @param avsobj 		avstage 对象句柄 
 * @param type_id 		数据类型[系统流/音频/视频]
 * @param buf			返回buf指针
 * @param size			[in]需要的大小 [out]实际返回的大小
 * @return 				0 成功, -2 不接受type_id指定的数据, 其它 错误号
 */
int sw_avstage_get_buffer(swavstage_t* avsobj, int type_id, uint8_t** buf, int* size);

/**
 * @brief 				提交添充好的数据到AVStage
 * @param avsobj 		avstage 对象句柄 
 * @param type_id 		数据类型[系统流/音频/视频]
 * @param timestamp 	type_id为ES流时为时间戳[45K]
 * @param buf			提交的buf指针
 * @param size			提交数buf大小
 * @param skip			跳过无效数据的大小
 * @return 				0 成功 其它 错误号
 */
int sw_avstage_put_buffer(swavstage_t* avsobj, int type_id, int64_t timestamp, uint8_t* buf, int size, int skip);

/**
 * @brief 				获取缓冲区状态
 * @param avsobj 		avstage 对象句柄 
 * @param totalsize 	缓冲区总大小
 * @param datasize 		已经存放数据大小
 * @return 				0 成功 其它 错误号
 */
int sw_avstage_get_buffer_status(swavstage_t* avsobj, uint32_t* totalsize, uint32_t* datasize);

/**
 * @brief 				设置状态
 * @param avsobj 		avstage 对象句柄 
 * @param status_id 	状态类型
 * @param param 		状态参数
 * @param param 		状态扩展参数
 * @return 				0 成功 其它 错误号
 */
int sw_avstage_set_status(swavstage_t* avsobj, int status_id, unsigned long param, unsigned long ext_param);

/**
 * @brief 				获取属性
 * @param avsobj 		avstage 对象句柄 
 * @param property_id 	属性类型
 * @param value 		存放属性的内存
 * @param size	 		存放属性的内存大小
 * @return 				0 成功 其它 错误号
 */
int sw_avstage_get_property(swavstage_t* avsobj, swavs_property_e property_id, void *value, int size);


/**
 * @brief 				设置属性
 * @param avsobj 		avstage 对象句柄 
 * @param property_id 	属性类型
 * @param value 		属性值内存
 * @param size	 		属性值内存大小
 * @return 				0 成功 其它 错误号
 */
int sw_avstage_set_property(swavstage_t* avsobj, swavs_property_e property_id, void *value, int size);


/**
 * @brief 				设置事件回调处理函数
 * @param avsobj 		avstage 对象句柄 
 * @param on_event 		回调函数指针
 * @param handle 		用户句柄
 * @return 				无
 */
void sw_avstage_set_event_callback(swavstage_t* avsobj, avs_event_callback on_event, void *handle);


const char* sw_avstage_status2str(uint32_t status_id);
const char* sw_avstage_property2str(uint32_t property_id);

int sw_avstage_inner_init(swavstage_t* avsobj);
void sw_avstage_inner_release(swavstage_t* avsobj);

#ifdef __cplusplus
}
#endif

#endif

